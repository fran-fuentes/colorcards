﻿using ColorCards.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ColorCards.Windows.Models
{
    public class CardModel
    {
        private ColorCard innerCard;

        public GridStatus Status { get; set; }
        public ColorCard Card
        {
            get { return innerCard ?? ColorCard.CardDefault(); }
            set { innerCard = value; }
        }

        public SolidColorBrush ColorBrush { get {
                return new SolidColorBrush(Color.FromArgb(Card.InnerColor.A, Card.InnerColor.R, Card.InnerColor.G, Card.InnerColor.B));
            }
        }

    }
}
