﻿using ColorCards.Business.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ColorCards.Windows.Models
{
    public class GridStatus: INotifyPropertyChanged
    {
        //Number of elements needed
        public int Needed { get; set; }

        //Object for rules
        public ICardsBusiness BusinessRules { get; set; }

        //Is there a card opening right now?
        public bool IsOpening { get; set; }

        private int score;
        public int Score
        {
            get { return score; }
            set { score = value; NotifyPropertyChanged(); }
        }

        //Victory
        private bool victory;

        public bool Victory
        {
            get { return victory; }
            set {
                victory = value;
                NotifyPropertyChanged();
            }
        }

        //The private collection of opened cards
        private ICollection<CardEffects> listOpenedCards;

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ICollection<CardEffects> ListOpenedCards
        {
            get {
                if (listOpenedCards == null)
                    listOpenedCards = new List<CardEffects>();
                return listOpenedCards;
            }
        }

        public int NumberColors { get; internal set; }
    }
}
