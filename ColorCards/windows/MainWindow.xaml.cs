﻿using ColorCards.Windows.Windows;
using System.Windows;

namespace ColorCards.Windows
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnInstructions_Click(object sender, RoutedEventArgs e)
        {
            var v = new Instructions();
            v.ShowDialog();
        }

        private void btnNewGame_Click(object sender, RoutedEventArgs e)
        {
            var w = new Board(this);
            this.Hide();
            w.Show();
        }
    }

}