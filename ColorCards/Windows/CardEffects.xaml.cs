﻿using ColorCards.Business.Models;
using ColorCards.Windows.Models;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace ColorCards.Windows
{
    /// <summary>
    /// Lógica de interacción para UserControl1.xaml
    /// </summary>
    public partial class CardEffects : UserControl
    {
        private CardModel Card { get; set; }
        public CardEffects(CardModel d)
        {
            InitializeComponent();
            this.DataContext = d;
            Card = d;
        }


        /// <summary>
        /// Here, for a better animation, the event controller of the game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CardAnimation_Completed(object sender, EventArgs e)
        {
            var s = Card.Status;

                //Add card and check if there's a good result
                s.ListOpenedCards.Add(this);
                var statusNow = s.BusinessRules.GetResult(s.ListOpenedCards.Select(m => m.Card.Card).ToArray(), s.Needed);
            switch (statusNow)
            {
                case GoalStatus.NOT_YET:
                    //Do Nothing
                    break;
                case GoalStatus.FAIL:
                    //close
                    FailActions(s);
                    break;
                case GoalStatus.GOAL:
                    //Remove cards
                    Thread.Sleep(2000);
                    foreach (var card in s.ListOpenedCards)
                    {
                        card.Visibility = System.Windows.Visibility.Hidden;
                    }
                    s.ListOpenedCards.Clear();
                    s.Score = s.Score + 1;
                    s.Victory = s.NumberColors == s.Score;
                    break;
            }
            Card.Status.IsOpening = false;
        }

        private static void FailActions(GridStatus s)
        {
            AnimateToClose(s);
            s.ListOpenedCards.Clear();
        }

        private static void AnimateToClose(GridStatus s)
        {
            Thread.Sleep(500);
            DoubleAnimation restoreAnimation = new DoubleAnimation();
            restoreAnimation.From = 0;
            restoreAnimation.To = 1;
            restoreAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            restoreAnimation.AutoReverse = false;
            foreach (var c in s.ListOpenedCards)
            {
                c.distor.BeginAnimation(ScaleTransform.ScaleXProperty, restoreAnimation);
            }

        }



        /// <summary>
        /// No more of one card moving
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TriggerB_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (Card.Status.IsOpening)
            {
                e.Handled = true;
                return;
            }
            else
                Card.Status.IsOpening = true;

        }
    }
}
