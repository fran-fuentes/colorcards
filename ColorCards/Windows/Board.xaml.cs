﻿using ColorCards.Business.Contracts;
using ColorCards.Business.Models;
using ColorCards.Business.Operators;
using ColorCards.Windows.Models;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace ColorCards.Windows.Windows
{

    /// <summary>
    /// Lógica de interacción para Board.xaml
    /// </summary>
    public partial class Board : Window
    {

        //Number of colors used
        private const int numberColors = 8;
        //Number of elements of the same color needed for scoring a point
        private const int numberNeeded = 2;

        //The parent windows, just for closing and opening it.
        private MainWindow Mn { get; set; }
        //The business object, at last
        private ICardsBusiness icb = new CardsBusiness();
        private GridStatus _gs = new GridStatus();
        public GridStatus GS
        {
            get
            {
                return _gs;
            }
            set
            {
                _gs = value;
            }
        }

        public Board(MainWindow parent)
        {
            InitializeComponent();
            lblVictory.Visibility = Visibility.Hidden;
            this.Mn = parent;
            //this.c1.rColor.Fill = new SolidColorBrush(Color.FromRgb(23,67,23));

            //1.- Get the array of colors
            ColorCard[] list = icb.GetCards(numberColors, numberNeeded);
            //2.- Create the cards
            GS.BusinessRules = icb;
            GS.Needed = numberNeeded;
            GS.NumberColors = numberColors;
            GS.Victory = false;
            var lCard = list.Select(m =>
            {
                var d = new CardModel
                {
                    Card = m,
                    Status = GS
                };
                return new CardEffects(d);
            });
            //3.- Positioning the cards on the board.
            var ch = this.grdBoard.Children;
            var columns = this.grdBoard.ColumnDefinitions.Count();
            var rows = this.grdBoard.RowDefinitions.Count();
            for (int i = 0; i<lCard.Count();i ++)
            {
                var u = lCard.ElementAt(i);
                ch.Add(u);
                var c = i % columns;
                var r = (i / (rows - 1)) + 1;
                Grid.SetColumn(u, c);
                Grid.SetRow(u, r);
            }
        }

        private void WBoard_Closed(object sender, EventArgs e)
        {

            this.Close();
            this.Mn.Show();
        }


    }

}
