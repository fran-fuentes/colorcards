﻿using ColorCards.Business.Models;

namespace ColorCards.Business.Contracts
{
    public interface ICardsBusiness
    {
        ColorCard[] GetCards(int numberColors, int numberNeeded);
        GoalStatus GetResult(ColorCard[] status, int numberNeeded);
    }
}
