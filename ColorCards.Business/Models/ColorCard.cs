﻿using System.Collections.Generic;
using System.Drawing;

namespace ColorCards.Business.Models
{
    public class ColorCard
    {
        public static ColorCard CardDefault ()
        {
            return new ColorCard()
            {
                InnerColor = Color.Azure
            };
        }
        public Color InnerColor { get; set; }
        /// <summary>
        /// Get the color as string
        /// </summary>
        /// <returns></returns>
        public string GetInnerColor()
        {
            return (InnerColor != null) ? InnerColor.ToString() : "#FF470D0D";
            
        }
        public override bool Equals(object obj)
        {
            bool dev = false;
            if (obj is ColorCard to)
            {
                dev = this.InnerColor.Equals(to.InnerColor);
            }
            return dev;
        }

        public override int GetHashCode()
        {
            return 1013400870 + EqualityComparer<Color>.Default.GetHashCode(InnerColor);
        }
    }
}
