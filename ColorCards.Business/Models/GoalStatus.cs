﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ColorCards.Business.Models
{
    public enum GoalStatus
    {
        FAIL,
        NOT_YET,
        GOAL
    }
}
