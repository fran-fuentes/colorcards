﻿using ColorCards.Business.Contracts;
using System.Collections.Generic;
using System.Drawing;
using System;
using ColorCards.Business.Models;
using System.Linq;

namespace ColorCards.Business.Operators
{
    public class CardsBusiness: ICardsBusiness
    {
        /// <summary>
        /// The base colors.
        /// </summary>
        private ICollection<Color> colorsOfMyRainbow = new List<Color>() {
            Color.Gold,
            Color.Beige,
            Color.BurlyWood,
            Color.IndianRed,
            Color.Brown,
            Color.HotPink,
            Color.GreenYellow,
            Color.Gainsboro,
            Color.DarkMagenta
        };
        public CardsBusiness()
        {

        }

        public ColorCard[] GetCards(int numberColors, int numberNeeded)
        {
            ColorCard[] dev = null;
            if (numberColors <= colorsOfMyRainbow.Count)
            {
                var r = new Random();
                var d = colorsOfMyRainbow.ToArray();
                r.Shuffle(d);
                var e = d.Take(numberColors);
                List<Color> final = new List<Color>();
                
                for (int i = 0; i < numberNeeded; i++)
                {
                    final.AddRange(e);
                }
                var final2 = final.ToArray();
                r.Shuffle(final2);
                dev = final2.Select( m => new ColorCard()
                {
                    InnerColor = m
                }).ToArray();

            }
            return dev;
        }
        public GoalStatus GetResult(ColorCard[] status, int numberNeeded)
        {
            GoalStatus dev = GoalStatus.NOT_YET;

            if (status.Count() > 0)
            {
                var p = status[0];
                var ok = status.All(m => m.Equals(p));
                if (ok)
                {
                    dev = status.Count() < numberNeeded ? GoalStatus.NOT_YET : (status.Count() == numberNeeded ? GoalStatus.GOAL : GoalStatus.FAIL);

                } else
                {
                    dev = GoalStatus.FAIL;
                }
            }
            return dev;
        }
    }
}
