﻿using System;

namespace ColorCards.Business.Operators
{
    ///<summary> Use it: 
    /// var array = new int[] {1, 2, 3, 4};
    /// new Random().Shuffle(array);
    /// </summary>
    /// <see>https://stackoverflow.com/questions/108819/best-way-to-randomize-an-array-with-net#110570</see>
    static class RandomExtensions
    {
        public static void Shuffle<T>(this Random rng, T[] array)
        {
            int n = array.Length;
            while (n > 1)
            {
                int k = rng.Next(n--);
                T temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }
    }
}
